import Vue from 'vue'
import vuex from 'vuex'
import axios from 'axios'
import { ToastProgrammatic as Toast } from 'buefy'

Vue.use(vuex, axios)

export default new vuex.Store({
  state: {
    movieList: [],
    movieDetail: {},
    totalItem: 0,
    isFetching: false,
  },
  actions: {
    loadMovie ({ commit }, params) {
      commit('SET_FETCHING')
      return axios
        .get(`http://www.omdbapi.com/?apikey=8898dbc0${params.replace('?', '&')}`)
        .then((res) => {
          const { Search, totalResults } = res.data
          commit('SET_MOVIES', Search)
          commit('SET_ITEMS', totalResults)
        })
        .catch((err) => {
          let message = err.message
          if (err.response) {
            if (err.response.data) {
              message = err.response.data.Error
            } else {
              message = err.response.statusText
            }
          }
          commit('SET_ERROR_FETCHING')
          Toast.open(message)
        })
    },
    loadMovieDetail ({ commit }, id) {
      commit('SET_FETCHING')
      return axios
        .get(`http://www.omdbapi.com/?apikey=8898dbc0&type=movie&plot=full&i=${id}`)
        .then((res) => {
          commit('SET_MOVIES_DETAIL', res.data)
        })
        .catch(err => {
          let message = err.message
          if (err.response) {
            if (err.response.data) {
              message = err.response.data.Error
            } else {
              message = err.response.statusText
            }
          }
          commit('SET_ERROR_FETCHING')
          Toast.open(message)
        })
    },
  },
  mutations: {
    SET_FETCHING: (state) => {
      return state.isFetching = true
    },
    SET_ERROR_FETCHING: (state) => {
      return state.isFetching = false
    },
    SET_MOVIES: (state, list) => {
      state.movieList = list
      state.isFetching = false
      return state
    },
    SET_MOVIES_DETAIL: (state, data) => {
      return state.movieDetail = data
    },
    SET_ITEMS: (state, total) => {
      return state.totalItem = total
    }
  },
  getters: {
    loading: state => {
      return state.isFetching
    }
  }
})