import Vue from 'vue'
import App from './App.vue'
import store from './store'
import Buefy from 'buefy'
import router from './router'
import 'buefy/dist/buefy.css'
// import './styles/index.scss'

Vue.use(Buefy, {
  defaultIconPack: 'fas'
})
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
