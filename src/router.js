import Vue from 'vue'
import Router from 'vue-router'
import ListPage from './pages/ListPage'
import DetailPage from './pages/DetailPage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'List',
      component: ListPage
    },
    {
      path: '/:id',
      name: 'Detail',
      props: true,
      component: DetailPage
    },
  ]
})